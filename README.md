# Сервис баннеров

## Стек
- php 8.2
- postgres 16
- Laravel Framework 11
- composer 2

## Запуск
Необходимо развернуть СУБД PostgreSQL 16, создать БД "banners", пользователя с логином "postgres" и паролем "123".
Для установки зависимостей composer:
```sh
composer insall
```
Для выполнения миграций Laravel для создания таблиц и демо-данных выполнить команду:
```sh
php artisan migrate
```
Для запуска Laravel выполнить команду:
```sh
php artisan serve
```
Сервис по умолчанию доступен по адресу http://127.0.0.1:8000

## Запросы
Коллекция запросов для Postman находится в файле postman_collection.json, Environment - в postman_environment.js

## Что можно улучишить?
- перевод кэша на redis, например
- удалить неиспользуемые файлы фреймворка
- покрыть код тестами
- попытаться выполнить доп.задания
- рефактор

Возможно, если бы у меня было больше опыта и времени, я бы это реализовал :)
