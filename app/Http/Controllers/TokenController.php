<?php

namespace App\Http\Controllers;

use App\Models\Token;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function index()
    {
        return Token::all();
    }

    public function store(Request $request)
    {
        try {
            $token = Token::create([
                'token' => $request->token,
                'is_admin' => $request->is_admin ?? false
            ]);
        } catch (\Exception $exception) {
            return response([
                'error' => $exception->getMessage()
            ])->setStatusCode(400);
        }

        return $token;
    }

    public function show(string $id)
    {
        return Token::find($id);
    }

    public function update(Request $request, string $id)
    {
        $token = Token::find($id);
        $token->token = $request->input('token');
        $token->is_admin = $request->input('is_admin');
        $token->save();

        return $token;
    }

    public function destroy(string $id)
    {
        $token = Token::find($id);
        $token->delete();
    }
}
