<?php

namespace App\Http\Controllers;

use App\Http\Middleware\PreventRequestsDuringMaintenance;
use App\Models\Tag;
use Illuminate\Http\Request;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class TagController extends Controller
{
    public function index()
    {
        return Tag::all();
    }

    public function store(Request $request)
    {
        if (!empty($request->input('name'))) {
            $tag = new Tag();
            $tag->name = $request->input('name');
            $tag->save();

            return $tag;
        }

        return response([
            'error' => 'Empty name for tag'
        ])->setStatusCode(400);
    }

    public function show(string $id)
    {
        try {
            return Tag::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Tag not found',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ], 400);
        }
    }

    public function update(Request $request, string $id)
    {
        if (!empty($request->input('name'))) {
            try {
                $tag = Tag::findOrFail($id);
                $tag->name = $request->input('name');
                $tag->save();

                return $tag;
            } catch (ModelNotFoundException $e) {
                return response()->json([
                    'error' => 'Tag not found',
                ], 404);
            } catch (\Exception $e) {
                return response()->json([
                    'error' => $e->getMessage(),
                ], 400);
            }
        }

        return response()->json([
            'error' => 'Empty name for tag',
        ], 400);
    }

    public function destroy(string $id)
    {
        try {
            $tag = Tag::findOrFail($id);
            $tag->delete();

            return response()->json('',204);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Tag not found',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ], 400);
        }
    }
}
