<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Cache;


class BannerController extends Controller
{
    public function index(Request $request)
    {
        $banners = Banner::query();

        if ($request->has('feature_id')) {
            $featureId = $request->integer('feature_id');
            $banners = $banners->where('feature_id', $featureId);
        }

        if ($request->has('tag_id')) {
            $tagId = $request->integer('tag_id');
            $banners = $banners->whereJsonContains('tag_ids', $tagId);
        }

        if ($request->has('limit')) {
            $limit = $request->integer('limit');
            $banners = $banners->limit($limit);
        }

        if ($request->has('offset')) {
            $offset = $request->integer('offset');
            $banners = $banners->offset($offset);
        }

        return $banners->get();
    }

    public function store(Request $request)
    {
        $tagIds = $request->input('tag_ids');
        $featureId = $request->input('feature_id');
        $content = $request->json('content');
        $isActive = $request->input('is_active') ?? false;

        if (empty($tagIds) || empty($featureId) || empty($content)) {
            return response(['error' => 'incorrect data'], 400);
        }

        if (!Banner::isBannerExists($featureId, $tagIds)) {
            $banner = new Banner();
            $banner->tag_ids = $tagIds;
            $banner->feature_id = $featureId;
            $banner->content = json_encode($content);
            $banner->is_active = $isActive;

            $banner->save();

            return response(['banner_id' => $banner->id], 201);

        } else {
            return response(['error' => 'duplicate pair "feature_id" + one of "tag_ids'], 400);
        }
    }

    public function update(Request $request, string $id)
    {
        try {
            $shouldPatchBanner = false;
            $banner = Banner::findOrFail($id);

            if (($request->has('tag_ids') && $banner->tag_ids != $request->input('tag_ids'))
                || ($request->has('feature_id') && $banner->feature_id != $request->input('feature_id'))) {
                $featureId = $request->input('feature_id') ?? $banner->feature_id;
                $tagIds = array_diff($request->input('tag_ids'), $banner->tag_ids) ?: $request->input('tag_ids');

                if (!Banner::isBannerExists($featureId, $tagIds)) {
                    $banner->tag_ids = $tagIds;
                    $banner->feature_id = $featureId;
                    $shouldPatchBanner = true;
                } else {
                    return response(['error' => 'duplicate pair "feature_id" + one of "tag_ids'], 400);
                }
            }

            if ($request->has('content')) {
                $content = $request->json('content');
                $banner->content = json_encode($content);
                $shouldPatchBanner = true;
            }

            if ($request->has('is_active')) {
                $isActive = $request->boolean('is_active');
                $banner->is_active = $isActive;
                $shouldPatchBanner = true;
            }

            if ($shouldPatchBanner) {
                $banner->save();
            }

        } catch (ModelNotFoundException $e) {
            return response(['error' => 'banner not found'], 404);
        }
    }

    public function destroy(string $id)
    {
        try {
            $banner = Banner::findOrFail($id);
            $banner->delete();

            return response()->json('',204);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Banner not found',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ], 400);
        }
    }

    public function getBannerForUser(Request $request)
    {
        if (!$request->has('feature_id') || !$request->has('tag_id')) {
            return response(['error' => 'incorrect data'], 400);
        }

        $featureId = $request->integer('feature_id');
        $tagId = $request->integer('tag_id');
        $getFromDb = $request->boolean('use_last_revision');

        $cacheKey = 'banner-' . $featureId . '-' . $tagId;

        if (!$getFromDb) {
            $bannerFromCache = Cache::get($cacheKey);

            if (!empty($bannerFromCache)) {
                return $bannerFromCache;
            }
        }

        $banner = Banner::query()
            ->where('feature_id', $featureId)
            ->whereJsonContains('tag_ids', $tagId)
            ->where('is_active', true);

        $bannerResult = $banner->get()->first();

        if (empty($bannerResult)) {
            return response('', 404);
        }

        Cache::put($cacheKey, $bannerResult, 300);

        return $bannerResult;
    }
}
