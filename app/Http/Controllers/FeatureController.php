<?php

namespace App\Http\Controllers;

use App\Models\Feature;
use Illuminate\Http\Request;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class FeatureController extends Controller
{
    public function index()
    {
        return Feature::all();
    }

    public function store(Request $request)
    {
        if (!empty($request->input('name'))) {
            $feature = new Feature();
            $feature->name = $request->input('name');
            $feature->save();

            return $feature;
        }

        return response([
            'error' => 'Empty name for feature'
        ])->setStatusCode(400);
    }

    public function show(string $id)
    {
        try {
            return Feature::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Feature not found',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ], 400);
        }
    }

    public function update(Request $request, string $id)
    {
        if (!empty($request->input('name'))) {
            try {
                $feature = Feature::findOrFail($id);
                $feature->name = $request->input('name');
                $feature->save();

                return $feature;
            } catch (ModelNotFoundException $e) {
                return response()->json([
                    'error' => 'Feature not found',
                ], 404);
            } catch (\Exception $e) {
                return response()->json([
                    'error' => $e->getMessage(),
                ], 400);
            }
        }

        return response()->json([
            'error' => 'Empty name for feature',
        ], 400);
    }

    public function destroy(string $id)
    {
        try {
            $feature = Feature::findOrFail($id);
            $feature->delete();

            return response()->json('',204);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Feature not found',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ], 400);
        }
    }
}
