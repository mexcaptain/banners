<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Token;
use Illuminate\Http\Request;

class VerifyToken
{

    public function handle($request, Closure $next)
    {
        $tokenValue = $request->input('token');

        if (empty($tokenValue)) {
            return response('', 401);
        }

        $token = Token::query()->where(['token' => $tokenValue])->get()->first();

        if (empty($token)) {
            return response('', 401);
        }

        return $next($request->merge(['tokenObject' => $token]));
    }
}
