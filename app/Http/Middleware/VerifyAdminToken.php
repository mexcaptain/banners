<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Token;

class VerifyAdminToken
{

    public function handle($request, Closure $next)
    {
        $token = $request->input('tokenObject');

        if (!$token->is_admin) {
            return response('', 403);
        }

        return $next($request);
    }
}
