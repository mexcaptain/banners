<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    protected $fillable = [
        'content',
        'is_active',
        'feature_id',
        'tag_ids'
    ];

    protected $casts = [
        'tag_ids' => 'array',
    ];

    public static function isBannerExists(int $featureId, array $tagIds)
    {
        $banners = self::query()->where('feature_id', $featureId);

        $banners->where(function ($banners) use ($tagIds) {
            foreach ($tagIds as $key => $tagId) {
                if ($key === 0) {
                    $banners = $banners->whereJsonContains('tag_ids', $tagId);
                } else {
                    $banners = $banners->orWhereJsonContains('tag_ids', $tagId);
                }
            }
        });

        $banners = $banners->get();

        return $banners->count() > 0;
    }
}
