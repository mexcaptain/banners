<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Token
 *
 * @mixin Eloquent
 */
class Token extends Model
{
    use HasFactory;

    protected $fillable = [
        'token',
        'is_admin'
    ];
}
