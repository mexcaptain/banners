<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TokenController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\FeatureController;
use App\Http\Controllers\BannerController;
use App\Http\Middleware\VerifyToken;
use App\Http\Middleware\VerifyAdminToken;




Route::middleware([VerifyToken::class])->group(function () {
    Route::middleware([VerifyAdminToken::class])->group(function () {
        Route::apiResource('/token', TokenController::class);
        Route::apiResource('/tag', TagController::class);
        Route::apiResource('/feature', FeatureController::class);
        Route::apiResource('/banner', BannerController::class);
    });
    Route::get('user_banner', [BannerController::class, 'getBannerForUser']);
});

