<?php

use App\Models\Banner;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->json('content');
            $table->boolean('is_active');
            $table->integer('feature_id');
            $table->json('tag_ids');
            $table->timestamps();
        });

        Banner::create([
            'tag_ids' => [1, 2],
            'feature_id' => 1,
            'content' => json_encode([
                'title' => 'some title',
                'text' => 'some text',
                'url' => 'some url'
            ]),
            'is_active' => true
        ]);
        Banner::create([
            'tag_ids' => [3, 4],
            'feature_id' => 1,
            'content' => json_encode([
                'title' => 'some title 2',
                'text' => 'some text 2',
                'url' => 'some url 2'
            ]),
            'is_active' => true
        ]);

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('banners');
    }
};
