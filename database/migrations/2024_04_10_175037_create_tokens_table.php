<?php

use App\Models\Token;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('tokens', function (Blueprint $table) {
            $table->id();
            $table->string('token')->unique();
            $table->boolean('is_admin')->default(false);
            $table->timestamps();
        });

        Token::create(['token' => 'token1', 'is_admin' => true]);
        Token::create(['token' => 'token2', 'is_admin' => false]);
        Token::create(['token' => 'token3', 'is_admin' => false]);
        Token::create(['token' => 'token4', 'is_admin' => false]);
    }

    public function down(): void
    {
        Schema::dropIfExists('tokens');
    }
};
