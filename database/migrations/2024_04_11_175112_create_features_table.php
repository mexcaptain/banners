<?php

use App\Models\Feature;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('features', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Feature::create(['name' => 'auto']);
        Feature::create(['name' => 'realty']);
        Feature::create(['name' => 'job']);
    }

    public function down(): void
    {
        Schema::dropIfExists('features');
    }
};
