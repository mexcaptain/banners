<?php

use App\Models\Tag;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Tag::create(['name' => 'man']);
        Tag::create(['name' => 'student']);
        Tag::create(['name' => 'turbo_buyer']);
    }

    public function down(): void
    {
        Schema::dropIfExists('tags');
    }
};
